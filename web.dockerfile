FROM nginx:latest

ADD ./laravelVHost.conf /etc/nginx/conf.d/default.conf
COPY . /var/www
WORKDIR /var/www
